import { toText, splitContainer } from '@inkylabs/remark-utils'
import { visit } from 'unist-util-visit'

// TODO: Remove uses of labelNode and contentNode.
export default (opts) => {
  return (root, f) => {
    visit(root, ['text-cite', 'text-Cite'], (node, index, parent) => {
      node.type = node.type.substring(5)
      if (node.attributes.p) {
        node.pages = node.attributes.p.replace(/-/g, '–')
      }
      node.nohyper = 'nohyper' in node.attributes
    })
    visit(root, 'text-ref', (node, index, parent) => {
      node.type = 'ref'
      node.value = toText(node, f)
    })
    visit(root, 'text-span', (node, index, parent) => {
      node.type = 'span'
    })
    visit(root, 'text-tex', (node, index, parent) => {
      node.type = 'tex'
      node.value = toText(node, f)
      delete node.children
    })
    visit(root, 'leaf-label', (node, index, parent) => {
      node.type = 'label'
      node.value = toText(node, f)
    })
    visit(root, 'leaf-tex', (node, index, parent) => {
      node.type = 'tex'
      node.value = toText(node, f)
      delete node.children
    })
    visit(root, 'container-definition', (node, index, parent) => {
      node.type = 'definition'
    })
    visit(root, 'container-tex', (node, index, parent) => {
      node.type = 'tex'
      node.value = toText(node, f)
      delete node.children
    })
    visit(root, 'container-attribution', (node, index, parent) => {
      node.type = 'attribution'
    })
    visit(root, 'container-texenv', (node, index, parent) => {
      node.type = 'texenv'
      node.name = toText(node.labelNode, f)
      node.children = node.contentsNode.children
    })
    visit(root, 'container-figure', (node, index, parent) => {
      node.type = 'figure'
      // eslint-disable-next-line no-unused-vars
      const [label, _] = splitContainer(node, f)
      if (label) {
        label.type = 'caption'
      } else {
        visit(node, 'container-caption', (n, i, p) => {
          n.type = 'caption'
          const cs = n.children
          if (cs.length === 1 && cs[0].type === 'paragraph') n.children = cs[0].children
        })
      }
      node.figpos = node.attributes.pos
    })
    const types = ['container-comment', 'container-todo', 'container-TODO']
    visit(root, types, (node, index, parent) => {
      node.type = 'comment'
    })
  }
}
